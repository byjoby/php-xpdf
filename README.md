# php-xpdf

A wrapper for using Xpdf command-line tools from PHP in an object-oriented way.
Naturally, this library requires that you have exec() enabled in your environment,
so it may not be possible to use it on many shared hosts.

## Licenses

### php-xpdf
Copyright (C) 2019  Joby Elliott

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Xpdf

The xpdf package is open source, dual licensed under GPL v2 and GPL v3. 
You can distribute derivatives of Xpdf under any of (1) GPL v2 only, (2) GPL v3 only, or (3) GPL v2 or v3.

See https://www.xpdfreader.com/opensource.html