<?php
/* php-xpdf | https://gitlab.com/byjoby/php-xpdf | GPL-3.0-only */
namespace ByJoby\XPDF;

class Environment
{
    protected $os = null;
    protected $bits = null;

    public function ready()
    {
        return $this->os() && $this->bits();
    }

    protected function detectOS()
    {
        if (substr(PHP_OS,0,3) == 'WIN') {
            return 'windows';
        }
        if (substr(PHP_OS,0,3) == 'LIN') {
            return 'linux';
        }
        if (substr(PHP_OS,0,3) == 'DAR') {
            return 'apple';
        }
        return null;
    }

    protected function detectBits()
    {
        return (PHP_INT_SIZE * 8);
    }

    public function os($set = null)
    {
        if ($set !== null) {
            $this->os = $set;
        }
        if (!$this->os) {
            $this->os = $this->detectOS();
        }
        return $this->os;
    }

    public function bits($set = null)
    {
        if ($set !== null) {
            $this->bits = $set;
        }
        if (!$this->bits) {
            $this->bits = $this->detectBits();
        }
        return $this->bits;
    }
}
