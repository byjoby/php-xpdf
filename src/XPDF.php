<?php
/* php-xpdf | https://gitlab.com/byjoby/php-xpdf | GPL-3.0-only */
namespace ByJoby\XPDF;

class XPDF implements XPDFInterface
{
    protected $file;

    public function __construct($file)
    {
        $file = realpath($file);
        if (!file_exists($file)) {
            throw new \Exception('File not found: '.$file);
        }
        if (!$this->env()->ready()) {
            throw new \Exception('XPDF Environment not ready. You may need to manually specify OS information or binary paths.');
        }
        $this->file = $file;
    }

    public static function env() : Environment
    {
        static $env = null;
        if (!$env) {
            $env = new Environment();
        }
        return $env;
    }
}
