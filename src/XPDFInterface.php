<?php
/* php-xpdf | https://gitlab.com/byjoby/php-xpdf | GPL-3.0-only */
namespace ByJoby\XPDF;

interface XPDFInterface
{
    /**
     * Returns a static Environment object that can be used to detect or
     * override information about the current environment. This object is where
     * the locations of the binaries comes from.
     *
     * @return Environment
     */
    public static function env() : Environment;
}
